DROP TABLE IF EXISTS `pay_alipay_config`;
CREATE TABLE `pay_alipay_config`
(
    `appId`           varchar(100)        NOT NULL COMMENT '应用ID',
    `appName`         varchar(255)        NOT NULL COMMENT '应用名称',
    `privateKey`      longtext            NOT NULL COMMENT '用户私钥',
    `alipayPublicKey` longtext            NOT NULL COMMENT '支付宝公钥',
    `enable`          bit(1)              NOT NULL DEFAULT b'1' COMMENT '启用标志',
    `enableDevEnv`    bit(1)              NOT NULL DEFAULT b'0' COMMENT '启用测试环境标志',
    `version`         bigint(20) unsigned NOT NULL DEFAULT '1' COMMENT '乐观锁版本',
    PRIMARY KEY (`appId`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='支付-支付宝支付配置';

DROP TABLE IF EXISTS `pay_log`;
CREATE TABLE `pay_log`
(
    `payLogId`        bigint(20) unsigned                            NOT NULL COMMENT '支付记录ID',
    `payOrderId`      bigint(20) unsigned                            NOT NULL COMMENT '支付订单ID',
    `orderTitle`      varchar(255)                                   NOT NULL COMMENT '订单标题',
    `businessGroup`   varchar(100)                                   NOT NULL COMMENT '业务分组',
    `businessId`      varchar(100)                                   NOT NULL COMMENT '业务ID',
    `payChannel`      varchar(100)                                   NOT NULL COMMENT '支付渠道',
    `amount`          decimal(65, 2) unsigned                        NOT NULL COMMENT '金额',
    `payStatus`       enum ('WAIT_PAY','PAYED','TIMEOUT','CANCELED') NOT NULL DEFAULT 'WAIT_PAY' COMMENT '支付状态',
    `createTime`      datetime                                       NOT NULL COMMENT '创建时间',
    `payTimeoutTime`  datetime                                       NOT NULL COMMENT '支付超时时间',
    `payedTime`       datetime                                                DEFAULT NULL COMMENT '支付完成时间',
    `payCancelTime`   datetime                                                DEFAULT NULL COMMENT '支付取消时间',
    `extend`          json                                           NOT NULL COMMENT '扩展信息',
    `exceptionFlag`   bit(1)                                         NOT NULL DEFAULT b'0' COMMENT '异常标志',
    `exceptionReason` varchar(255)                                            DEFAULT NULL COMMENT '异常原因',
    `version`         bigint(20) unsigned                            NOT NULL DEFAULT '1' COMMENT '乐观锁版本',
    PRIMARY KEY (`payLogId`) USING BTREE,
    KEY `business` (`businessGroup`, `businessId`) USING BTREE COMMENT '业务唯一ID'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='支付-支付记录';

DROP TABLE IF EXISTS `pay_log_notify`;
CREATE TABLE `pay_log_notify`
(
    `payLogNotifyId`  bigint(20)              NOT NULL COMMENT '支付记录通知ID',
    `payLogId`        bigint(20) unsigned     NOT NULL COMMENT '支付记录ID',
    `payOrderId`      bigint(20) unsigned     NOT NULL COMMENT '支付订单ID',
    `businessGroup`   varchar(100)            NOT NULL COMMENT '业务分组',
    `businessId`      varchar(100)            NOT NULL COMMENT '业务ID',
    `payChannel`      varchar(100)            NOT NULL COMMENT '支付渠道',
    `amount`          decimal(65, 2) unsigned NOT NULL COMMENT '金额',
    `outOrderSn`      varchar(255)            NOT NULL COMMENT '外部订单编号',
    `createTime`      datetime                NOT NULL COMMENT '创建时间',
    `exceptionFlag`   bit(1)                  NOT NULL DEFAULT b'0' COMMENT '异常标志',
    `exceptionReason` varchar(255)                     DEFAULT NULL COMMENT '异常原因',
    PRIMARY KEY (`payLogNotifyId`) USING BTREE,
    KEY `business` (`businessGroup`, `businessId`) USING BTREE COMMENT '业务唯一ID'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='支付-支付记录通知';

CREATE TABLE `pay_notify_original_log`
(
    `notifyOriginalLogId` bigint(20) unsigned NOT NULL COMMENT '通知原始记录ID',
    `payChannel`          varchar(100)        NOT NULL COMMENT '支付渠道',
    `createTime`          datetime            NOT NULL COMMENT '创建时间',
    `content`             longtext            NOT NULL COMMENT '内容',
    `result`              longtext            NOT NULL COMMENT '响应',
    PRIMARY KEY (`notifyOriginalLogId`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='支付-通知原始记录';

DROP TABLE IF EXISTS `pay_order`;
CREATE TABLE `pay_order`
(
    `payOrderId`          bigint(20) unsigned                            NOT NULL COMMENT '支付订单ID',
    `orderTitle`          varchar(255)                                   NOT NULL COMMENT '订单标题',
    `businessGroup`       varchar(100)                                   NOT NULL COMMENT '业务分组',
    `businessId`          varchar(100)                                   NOT NULL COMMENT '业务ID',
    `amount`              decimal(65, 2) unsigned                        NOT NULL COMMENT '金额',
    `payStatus`           enum ('WAIT_PAY','PAYED','TIMEOUT','CANCELED') NOT NULL DEFAULT 'WAIT_PAY' COMMENT '支付状态',
    `createTime`          datetime                                       NOT NULL COMMENT '创建时间',
    `payTimeoutTime`      datetime                                       NOT NULL COMMENT '支付超时时间',
    `payedTime`           datetime                                                DEFAULT NULL COMMENT '支付完成时间',
    `payCancelTime`       datetime                                                DEFAULT NULL COMMENT '支付取消时间',
    `exceptionFlag`       bit(1)                                         NOT NULL DEFAULT b'0' COMMENT '异常标志',
    `exceptionReason`     varchar(255)                                            DEFAULT NULL COMMENT '异常原因',
    `payedSucceedChannel` varchar(100)                                            DEFAULT NULL COMMENT '支付成功渠道',
    `version`             bigint(20) unsigned                            NOT NULL DEFAULT '1' COMMENT '乐观锁版本',
    PRIMARY KEY (`payOrderId`),
    UNIQUE KEY `business` (`businessGroup`, `businessId`) COMMENT '业务唯一ID'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='支付-支付订单';

CREATE TABLE `pay_wxpay_config`
(
    `appId`            varchar(100)        NOT NULL COMMENT '应用ID',
    `appName`          varchar(255)        NOT NULL COMMENT '应用名称',
    `mchId`            varchar(100)        NOT NULL COMMENT '微信支付商户号',
    `mchKey`           varchar(255)        NOT NULL COMMENT '微信支付商户密钥',
    `subAppId`         varchar(100)                 DEFAULT NULL COMMENT '服务商模式下的子商户公众账号ID',
    `subMchId`         varchar(100)                 DEFAULT NULL COMMENT '服务商模式下的子商户号',
    `keyContentBase64` longtext            NOT NULL COMMENT 'p12证书文件内容',
    `enable`           bit(1)              NOT NULL DEFAULT b'1' COMMENT '启用标志',
    `version`          bigint(20) unsigned NOT NULL DEFAULT '1' COMMENT '乐观锁版本',
    PRIMARY KEY (`appId`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='支付-微信支付配置';
