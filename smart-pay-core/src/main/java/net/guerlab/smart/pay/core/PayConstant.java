/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.core;

/**
 * 支付常量
 *
 * @author guer
 */
public interface PayConstant {

    /**
     * 业务分组最大长度
     */
    int BUSINESS_GROUP_MAX_LENGTH = 100;

    /**
     * 业务ID最大长度
     */
    int BUSINESS_ID_MAX_LENGTH = 100;

    /**
     * 订单标题最大长度
     */
    int ORDER_TITLE_MAX_LENGTH = 255;
}
