/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.core.searchparams;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.SearchModel;
import net.guerlab.spring.searchparams.SearchModelType;

import javax.persistence.Column;

/**
 * 支付宝支付配置搜索参数
 *
 * @author guer
 */
@Setter
@Getter
@Schema(name = "AlipayConfigSearchParams", description = "微信支付配置搜索参数")
public class AlipayConfigSearchParams extends AbstractSearchParams {

    /**
     * 应用ID
     */
    @Schema(description = "应用ID")
    private String appId;

    /**
     * 应用名称关键字
     */
    @Schema(description = "应用名称关键字")
    @SearchModel(SearchModelType.LIKE)
    @Column(name = "appName")
    private String appNameLike;

    /**
     * 启用标志
     */
    @Schema(description = "启用标志")
    private Boolean enable;

    /**
     * 启用测试环境标志
     */
    @Schema(description = "启用测试环境标志")
    private Boolean enableDevEnv;
}
