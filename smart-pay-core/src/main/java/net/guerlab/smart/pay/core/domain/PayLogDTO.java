/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.core.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import net.guerlab.smart.pay.core.enums.PayStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 支付记录
 *
 * @author guer
 */
@Data
@Schema(name = "PayLogDTO", description = "支付记录")
public class PayLogDTO {

    /**
     * 支付记录ID
     */
    @Schema(description = "支付记录ID")
    private Long payLogId;

    /**
     * 支付订单ID
     */
    @Schema(description = "支付订单ID")
    private Long payOrderId;

    /**
     * 订单标题
     */
    @Schema(description = "订单标题")
    private String orderTitle;

    /**
     * 业务分组
     */
    @Schema(description = "业务分组")
    private String businessGroup;

    /**
     * 业务ID
     */
    @Schema(description = "业务ID")
    private String businessId;

    /**
     * 支付渠道
     */
    @Schema(description = "支付渠道")
    private String payChannel;

    /**
     * 金额
     */
    @Schema(description = "金额")
    private BigDecimal amount;

    /**
     * 支付状态
     */
    @Schema(description = "支付状态")
    private PayStatus payStatus;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    /**
     * 支付超时时间
     */
    @Schema(description = "支付超时时间")
    private LocalDateTime payTimeoutTime;

    /**
     * 支付完成时间
     */
    @Schema(description = "支付完成时间")
    private LocalDateTime payedTime;

    /**
     * 支付取消时间
     */
    @Schema(description = "支付取消时间")
    private LocalDateTime payCancelTime;

    /**
     * 扩展信息
     */
    @Schema(description = "扩展信息")
    private PayLogExtends extend;

    /**
     * 异常标志
     */
    @Schema(description = "异常标志")
    private Boolean exceptionFlag;

    /**
     * 异常原因
     */
    @Schema(description = "异常原因")
    private String exceptionReason;
}
