/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.core.searchparams;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.OrderByType;
import net.guerlab.spring.searchparams.SearchModel;
import net.guerlab.spring.searchparams.SearchModelType;

import javax.persistence.Column;
import java.time.LocalDateTime;

/**
 * 通知原始记录搜索参数
 *
 * @author guer
 */
@Setter
@Getter
@Schema(name = "NotifyOriginalLogSearchParams", description = "通知原始记录搜索参数")
public class NotifyOriginalLogSearchParams extends AbstractSearchParams {

    /**
     * 通知原始记录ID
     */
    @Schema(description = "通知原始记录ID")
    private Long notifyOriginalLogId;

    /**
     * 支付渠道
     */
    @Schema(description = "支付渠道")
    private String payChannel;

    /**
     * 创建时间开始范围
     */
    @Schema(description = "创建时间开始范围")
    @Column(name = "createTime")
    @SearchModel(SearchModelType.GREATER_THAN_OR_EQUAL_TO)
    private LocalDateTime createTimeStartWith;

    /**
     * 创建时间结束范围
     */
    @Schema(description = "创建时间结束范围")
    @Column(name = "createTime")
    @SearchModel(SearchModelType.LESS_THAN_OR_EQUAL_TO)
    private LocalDateTime createTimeEndWith;

    /**
     * 通知原始记录ID排序方式
     */
    @Schema(hidden = true)
    @Column(name = "notifyOriginalLogId")
    @JsonIgnore
    private OrderByType notifyOriginalLogIdOrderByType = OrderByType.DESC;
}
