/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.core.searchparams;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.SearchModel;
import net.guerlab.spring.searchparams.SearchModelType;

import javax.persistence.Column;

/**
 * 微信支付配置搜索参数
 *
 * @author guer
 */
@Setter
@Getter
@Schema(name = "WxPayConfigSearchParams", description = "微信支付配置搜索参数")
public class WxPayConfigSearchParams extends AbstractSearchParams {

    /**
     * 应用ID
     */
    @Schema(description = "应用ID")
    private String appId;

    /**
     * 应用名称关键字
     */
    @Schema(description = "应用名称关键字")
    @SearchModel(SearchModelType.LIKE)
    @Column(name = "appName")
    private String appNameLike;

    /**
     * 微信支付商户号
     */
    @Schema(description = "微信支付商户号")
    private String mchId;

    /**
     * 服务商模式下的子商户公众账号ID
     */
    @Schema(description = "服务商模式下的子商户公众账号ID")
    private String subAppId;

    /**
     * 服务商模式下的子商户号
     */
    @Schema(description = "服务商模式下的子商户号")
    private String subMchId;

    /**
     * 启用标志
     */
    @Schema(description = "启用标志")
    private Boolean enable;
}
