/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.core.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 支付宝支付配置
 *
 * @author guer
 */
@Data
@Schema(name = "AlipayConfigDTO", description = "微信支付配置")
public class AlipayConfigDTO {

    /**
     * 应用ID
     */
    @Schema(description = "应用ID")
    private String appId;

    /**
     * 应用名称
     */
    @Schema(description = "应用名称")
    private String appName;

    /**
     * 用户私钥
     */
    @Schema(description = "用户私钥")
    private String privateKey;

    /**
     * 支付宝公钥
     */
    @Schema(description = "支付宝公钥")
    private String alipayPublicKey;

    /**
     * 启用标志
     */
    @Schema(description = "启用标志")
    private Boolean enable;

    /**
     * 启用测试环境标志
     */
    @Schema(description = "启用测试环境标志")
    private Boolean enableDevEnv;
}
