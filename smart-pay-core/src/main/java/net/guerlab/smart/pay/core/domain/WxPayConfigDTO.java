/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.core.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 微信支付配置
 *
 * @author guer
 */
@Data
@Schema(name = "WxPayConfigDTO", description = "微信支付配置")
public class WxPayConfigDTO {

    /**
     * 应用ID
     */
    @Schema(description = "应用ID")
    private String appId;

    /**
     * 应用名称
     */
    @Schema(description = "应用名称")
    private String appName;

    /**
     * 微信支付商户号
     */
    @Schema(description = "微信支付商户号")
    private String mchId;

    /**
     * 微信支付商户密钥
     */
    @Schema(description = "微信支付商户密钥")
    private String mchKey;

    /**
     * 服务商模式下的子商户公众账号ID
     */
    @Schema(description = "服务商模式下的子商户公众账号ID")
    private String subAppId;

    /**
     * 服务商模式下的子商户号
     */
    @Schema(description = "服务商模式下的子商户号")
    private String subMchId;

    /**
     * p12证书文件内容
     */
    @Schema(description = "p12证书文件内容")
    private String keyContentBase64;

    /**
     * 启用标志
     */
    @Schema(description = "启用标志")
    private Boolean enable;
}
