/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.core.searchparams;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.OrderByType;
import net.guerlab.spring.searchparams.SearchModel;
import net.guerlab.spring.searchparams.SearchModelType;

import javax.persistence.Column;
import java.time.LocalDateTime;

/**
 * 支付记录通知查询参数
 *
 * @author guer
 */
@Setter
@Getter
@Schema(name = "PayLogNotifySearchParams", description = "支付记录通知查询参数")
public class PayLogNotifySearchParams extends AbstractSearchParams {

    /**
     * 支付记录通知ID
     */
    @Schema(description = "支付记录通知ID")
    private Long payLogNotifyId;

    /**
     * 支付记录ID
     */
    @Schema(description = "支付记录ID")
    private Long payLogId;

    /**
     * 支付订单ID
     */
    @Schema(description = "支付订单ID")
    private Long payOrderId;

    /**
     * 业务分组
     */
    @Schema(description = "业务分组")
    private String businessGroup;

    /**
     * 业务ID
     */
    @Schema(description = "业务ID")
    private String businessId;

    /**
     * 支付渠道
     */
    @Schema(description = "支付渠道")
    private String payChannel;

    /**
     * 外部订单编号
     */
    @Schema(description = "外部订单编号")
    private String outOrderSn;

    /**
     * 创建时间开始范围
     */
    @Schema(description = "创建时间开始范围")
    @Column(name = "createTime")
    @SearchModel(SearchModelType.GREATER_THAN_OR_EQUAL_TO)
    private LocalDateTime createTimeStartWith;

    /**
     * 创建时间结束范围
     */
    @Schema(description = "创建时间结束范围")
    @Column(name = "createTime")
    @SearchModel(SearchModelType.LESS_THAN_OR_EQUAL_TO)
    private LocalDateTime createTimeEndWith;

    /**
     * 异常标志
     */
    @Schema(description = "异常标志")
    private Boolean exceptionFlag;

    /**
     * 异常原因关键字
     */
    @Schema(description = "异常原因关键字")
    @Column(name = "exceptionReason")
    @SearchModel(SearchModelType.LIKE)
    private String exceptionReasonLike;

    /**
     * 支付记录通知ID排序方式
     */
    @Schema(hidden = true)
    @Column(name = "payLogNotifyId")
    @JsonIgnore
    private OrderByType payLogNotifyIdOrderByType = OrderByType.DESC;
}
