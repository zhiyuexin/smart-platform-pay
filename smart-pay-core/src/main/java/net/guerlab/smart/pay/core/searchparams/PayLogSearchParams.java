/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.core.searchparams;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import net.guerlab.smart.pay.core.enums.PayStatus;
import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.OrderByType;
import net.guerlab.spring.searchparams.SearchModel;
import net.guerlab.spring.searchparams.SearchModelType;

import javax.persistence.Column;
import java.time.LocalDateTime;

/**
 * 支付记录搜索参数
 *
 * @author guer
 */
@Setter
@Getter
@Schema(name = "PayLogSearchParams", description = "支付记录搜索参数")
public class PayLogSearchParams extends AbstractSearchParams {

    /**
     * 支付记录ID
     */
    @Schema(description = "支付记录ID")
    private Long payLogId;

    /**
     * 支付订单ID
     */
    @Schema(description = "支付订单ID")
    private Long payOrderId;

    /**
     * 订单标题关键字
     */
    @Schema(description = "订单标题关键字")
    @SearchModel(SearchModelType.LIKE)
    @Column(name = "orderTitle")
    private String orderTitleLike;

    /**
     * 业务分组
     */
    @Schema(description = "业务分组")
    private String businessGroup;

    /**
     * 业务ID
     */
    @Schema(description = "业务ID")
    private String businessId;

    /**
     * 支付渠道
     */
    @Schema(description = "支付渠道")
    private String payChannel;

    /**
     * 支付状态
     */
    @Schema(description = "支付状态")
    private PayStatus payStatus;

    /**
     * 创建时间开始范围
     */
    @Schema(description = "创建时间开始范围")
    @Column(name = "createTime")
    @SearchModel(SearchModelType.GREATER_THAN_OR_EQUAL_TO)
    private LocalDateTime createTimeStartWith;

    /**
     * 创建时间结束范围
     */
    @Schema(description = "创建时间结束范围")
    @Column(name = "createTime")
    @SearchModel(SearchModelType.LESS_THAN_OR_EQUAL_TO)
    private LocalDateTime createTimeEndWith;

    /**
     * 支付超时时间开始范围
     */
    @Schema(description = "支付超时时间开始范围")
    @Column(name = "payTimeoutTime")
    @SearchModel(SearchModelType.GREATER_THAN_OR_EQUAL_TO)
    private LocalDateTime payTimeoutTimeStartWith;

    /**
     * 支付超时时间结束范围
     */
    @Schema(description = "支付超时时间结束范围")
    @Column(name = "payTimeoutTime")
    @SearchModel(SearchModelType.LESS_THAN_OR_EQUAL_TO)
    private LocalDateTime payTimeoutTimeEndWith;

    /**
     * 异常标志
     */
    @Schema(description = "异常标志")
    private Boolean exceptionFlag;

    /**
     * 异常原因关键字
     */
    @Schema(description = "异常原因关键字")
    @Column(name = "exceptionReason")
    @SearchModel(SearchModelType.LIKE)
    private String exceptionReasonLike;

    /**
     * 支付记录ID排序方式
     */
    @Schema(hidden = true)
    @Column(name = "payLogId")
    @JsonIgnore
    private OrderByType payLogIdOrderByType = OrderByType.DESC;
}
