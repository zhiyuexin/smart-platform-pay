/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.core.enums;

/**
 * 支付状态
 *
 * @author guer
 */
public enum PayStatus {
    /**
     * 等待支付
     */
    WAIT_PAY,
    /**
     * 支付完成
     */
    PAYED,
    /**
     * 支付超时
     */
    TIMEOUT,
    /**
     * 已取消
     */
    CANCELED
}
