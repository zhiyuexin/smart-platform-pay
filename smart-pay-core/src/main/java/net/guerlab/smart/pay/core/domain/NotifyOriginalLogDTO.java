/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.core.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 通知原始记录
 *
 * @author guer
 */
@Data
@Schema(name = "NotifyOriginalLogDTO", description = "通知原始记录")
public class NotifyOriginalLogDTO {

    /**
     * 通知原始记录ID
     */
    @Schema(description = "通知原始记录ID")
    private Long notifyOriginalLogId;

    /**
     * 支付渠道
     */
    @Schema(description = "支付渠道")
    private String payChannel;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    /**
     * 内容
     */
    @Schema(description = "内容")
    private String content;

    /**
     * 响应
     */
    @Schema(description = "响应")
    private String result;
}
