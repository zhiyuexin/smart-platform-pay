/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.service;

import net.guerlab.smart.pay.core.searchparams.AlipayConfigSearchParams;
import net.guerlab.smart.pay.service.entity.AlipayConfig;
import net.guerlab.smart.platform.server.service.BaseService;

/**
 * 支付宝支付配置服务
 *
 * @author guer
 */
public interface AlipayConfigService extends BaseService<AlipayConfig, String, AlipayConfigSearchParams> {

    /**
     * 应用名称最大长度
     */
    int APP_NAME_MAX_LENGTH = 255;

    /**
     * 获取实体类型
     *
     * @return 实体类型
     */
    @Override
    default Class<AlipayConfig> getEntityClass() {
        return AlipayConfig.class;
    }
}
