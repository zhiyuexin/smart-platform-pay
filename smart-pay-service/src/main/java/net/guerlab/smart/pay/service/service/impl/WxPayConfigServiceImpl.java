/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.service.impl;

import net.guerlab.smart.pay.core.exception.*;
import net.guerlab.smart.pay.core.searchparams.WxPayConfigSearchParams;
import net.guerlab.smart.pay.service.entity.WxPayConfig;
import net.guerlab.smart.pay.service.mapper.WxPayConfigMapper;
import net.guerlab.smart.pay.service.service.WxPayConfigService;
import net.guerlab.smart.platform.server.service.BaseServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 微信支付配置服务实现
 *
 * @author guer
 */
@Service
public class WxPayConfigServiceImpl extends BaseServiceImpl<WxPayConfig, String, WxPayConfigMapper, WxPayConfigSearchParams>
        implements WxPayConfigService {

    @Override
    protected void insertBefore(WxPayConfig entity) {
        String appId = StringUtils.trimToNull(entity.getAppId());
        String appName = StringUtils.trimToNull(entity.getAppName());
        String mchId = StringUtils.trimToNull(entity.getMchId());
        String mchKey = StringUtils.trimToNull(entity.getMchKey());
        String keyContentBase64 = StringUtils.trimToNull(entity.getKeyContentBase64());

        if (appId == null) {
            throw new WxPayConfigAppIdInvalidException();
        } else if (selectById(appId) != null) {
            throw new WxPayConfigAppIdRepeatException();
        }
        if (appName == null) {
            throw new WxPayConfigAppNameInvalidException();
        } else if (appName.length() > APP_NAME_MAX_LENGTH) {
            throw new WxPayConfigAppNameLengthErrorException();
        }
        if (mchId == null) {
            throw new WxPayConfigMchIdInvalidException();
        }
        if (mchKey == null) {
            throw new WxPayConfigMchKeyInvalidException();
        }
        if (keyContentBase64 == null) {
            throw new WxPayConfigKeyContentBase64InvalidException();
        }

        entity.setAppId(appId);
        entity.setAppName(appName);
        entity.setMchId(mchId);
        entity.setMchKey(mchKey);
        entity.setSubAppId(StringUtils.trimToNull(entity.getSubAppId()));
        entity.setSubMchId(StringUtils.trimToNull(entity.getSubMchId()));
        entity.setKeyContentBase64(keyContentBase64);
        if (entity.getEnable() == null) {
            entity.setEnable(true);
        }
    }

    @Override
    protected void updateBefore(WxPayConfig entity) {
        String appName = StringUtils.trimToNull(entity.getAppName());
        if (appName != null && appName.length() > APP_NAME_MAX_LENGTH) {
            throw new WxPayConfigAppNameLengthErrorException();
        }
        entity.setAppName(appName);
        entity.setMchId(StringUtils.trimToNull(entity.getMchId()));
        entity.setMchKey(StringUtils.trimToNull(entity.getMchKey()));
        entity.setSubAppId(StringUtils.trimToNull(entity.getSubAppId()));
        entity.setSubMchId(StringUtils.trimToNull(entity.getSubMchId()));
        entity.setKeyContentBase64(StringUtils.trimToNull(entity.getKeyContentBase64()));
    }
}
