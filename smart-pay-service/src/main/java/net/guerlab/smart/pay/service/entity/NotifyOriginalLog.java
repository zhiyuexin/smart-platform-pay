/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import net.guerlab.smart.pay.core.domain.NotifyOriginalLogDTO;
import net.guerlab.spring.commons.dto.DefaultConvert;

import java.time.LocalDateTime;

/**
 * 通知原始记录
 *
 * @author guer
 */
@Data
@TableName("pay_notify_original_log")
public class NotifyOriginalLog implements DefaultConvert<NotifyOriginalLogDTO> {

    /**
     * 通知原始记录ID
     */
    @TableId
    private Long notifyOriginalLogId;

    /**
     * 支付渠道
     */
    @TableField(value = "payChannel", updateStrategy = FieldStrategy.NEVER)
    private String payChannel;

    /**
     * 创建时间
     */
    @TableField(value = "createTime", updateStrategy = FieldStrategy.NEVER)
    private LocalDateTime createTime;

    /**
     * 内容
     */
    @TableField(value = "content", updateStrategy = FieldStrategy.NEVER)
    private String content;

    /**
     * 响应
     */
    @TableField(value = "result", updateStrategy = FieldStrategy.NEVER)
    private String result;
}
