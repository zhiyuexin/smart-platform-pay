/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.pay.core.domain.PayOrderDTO;
import net.guerlab.smart.pay.core.enums.PayStatus;
import net.guerlab.smart.platform.commons.entity.BaseEntity;
import net.guerlab.spring.commons.dto.DefaultConvert;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 支付订单
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pay_order")
public class PayOrder extends BaseEntity implements DefaultConvert<PayOrderDTO> {

    /**
     * 支付订单ID
     */
    @TableId
    private Long payOrderId;

    /**
     * 订单标题
     */
    @TableField(value = "orderTitle", updateStrategy = FieldStrategy.NEVER)
    private String orderTitle;

    /**
     * 业务分组
     */
    @TableField(value = "businessGroup", updateStrategy = FieldStrategy.NEVER)
    private String businessGroup;

    /**
     * 业务ID
     */
    @TableField(value = "businessId", updateStrategy = FieldStrategy.NEVER)
    private String businessId;

    /**
     * 金额
     */
    @TableField(value = "amount", updateStrategy = FieldStrategy.NEVER)
    private BigDecimal amount;

    /**
     * 支付状态
     */
    @TableField(value = "payStatus")
    private PayStatus payStatus;

    /**
     * 创建时间
     */
    @TableField(value = "createTime", updateStrategy = FieldStrategy.NEVER)
    private LocalDateTime createTime;

    /**
     * 支付超时时间
     */
    @TableField(value = "payTimeoutTime", updateStrategy = FieldStrategy.NEVER)
    private LocalDateTime payTimeoutTime;

    /**
     * 支付完成时间
     */
    private LocalDateTime payedTime;

    /**
     * 支付取消时间
     */
    @TableField(value = "payCancelTime", insertStrategy = FieldStrategy.NEVER)
    private LocalDateTime payCancelTime;

    /**
     * 异常标志
     */
    @TableField(value = "exceptionFlag")
    private Boolean exceptionFlag;

    /**
     * 异常原因
     */
    @TableField(value = "exceptionReason", insertStrategy = FieldStrategy.NEVER)
    private String exceptionReason;

    /**
     * 支付成功渠道
     */
    private String payedSucceedChannel;

}
