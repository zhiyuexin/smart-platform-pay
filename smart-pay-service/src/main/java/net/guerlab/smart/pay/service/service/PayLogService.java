/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.service;

import net.guerlab.smart.pay.core.domain.PayLogExtends;
import net.guerlab.smart.pay.core.searchparams.PayLogSearchParams;
import net.guerlab.smart.pay.service.entity.PayLog;
import net.guerlab.smart.platform.server.service.BaseFindService;

/**
 * 支付记录服务
 *
 * @author guer
 */
public interface PayLogService extends BaseFindService<PayLog, Long, PayLogSearchParams> {

    /**
     * 支付完成
     *
     * @param payLog
     *         支付记录
     */
    void payed(PayLog payLog);

    /**
     * 通过支付订单ID、支付渠道创建支付记录
     *
     * @param payOrderId
     *         支付订单ID
     * @param payChannel
     *         支付渠道
     * @param extend
     *         扩展信息
     * @return 支付记录
     */
    PayLog create(Long payOrderId, String payChannel, PayLogExtends extend);

    /**
     * 通过业务分组、业务ID、支付渠道创建支付记录
     *
     * @param businessGroup
     *         业务分组
     * @param businessId
     *         业务ID
     * @param payChannel
     *         支付渠道
     * @param extend
     *         扩展信息
     * @return 支付记录
     */
    PayLog create(String businessGroup, String businessId, String payChannel, PayLogExtends extend);

    /**
     * 标记异常
     *
     * @param payLogNotifyId
     *         支付记录通知ID
     * @param exceptionReason
     *         异常ID
     */
    void markException(Long payLogNotifyId, String exceptionReason);

    /**
     * 移除标记异常
     *
     * @param payLogNotifyId
     *         支付记录通知ID
     */
    void removeExceptionMark(Long payLogNotifyId);

    /**
     * 获取实体类型
     *
     * @return 实体类型
     */
    @Override
    default Class<PayLog> getEntityClass() {
        return PayLog.class;
    }
}
