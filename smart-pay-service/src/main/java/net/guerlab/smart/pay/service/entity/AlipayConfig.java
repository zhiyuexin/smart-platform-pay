/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.pay.core.domain.AlipayConfigDTO;
import net.guerlab.smart.platform.commons.entity.BaseEntity;
import net.guerlab.spring.commons.dto.DefaultConvert;

/**
 * 支付宝支付配置
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pay_alipay_config")
public class AlipayConfig extends BaseEntity implements DefaultConvert<AlipayConfigDTO> {

    /**
     * 应用ID
     */
    @TableId
    private String appId;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 用户私钥
     */
    private String privateKey;

    /**
     * 支付宝公钥
     */
    private String alipayPublicKey;

    /**
     * 启用标志
     */
    private Boolean enable;

    /**
     * 启用测试环境标志
     */
    private Boolean enableDevEnv;
}
