/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.pay.core.domain.WxPayConfigDTO;
import net.guerlab.smart.platform.commons.entity.BaseEntity;
import net.guerlab.spring.commons.dto.DefaultConvert;

/**
 * 微信支付配置
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pay_wxpay_config")
public class WxPayConfig extends BaseEntity implements DefaultConvert<WxPayConfigDTO> {

    /**
     * 应用ID
     */
    @TableId
    private String appId;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 微信支付商户号
     */
    private String mchId;

    /**
     * 微信支付商户密钥
     */
    private String mchKey;

    /**
     * 服务商模式下的子商户公众账号ID
     */
    private String subAppId;

    /**
     * 服务商模式下的子商户号
     */
    private String subMchId;

    /**
     * p12证书文件内容
     */
    private String keyContentBase64;

    /**
     * 启用标志
     */
    private Boolean enable;
}
