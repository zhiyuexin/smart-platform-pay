/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.guerlab.smart.pay.service.entity.PayLogNotify;
import org.apache.ibatis.annotations.Param;

/**
 * 支付记录通知Mapper
 *
 * @author guer
 */
public interface PayLogNotifyMapper extends BaseMapper<PayLogNotify> {

    /**
     * 标记异常
     *
     * @param payLogNotifyId
     *         支付记录通知ID
     * @param exceptionReason
     *         异常ID
     */
    void markException(@Param("payLogNotifyId") Long payLogNotifyId, @Param("exceptionReason") String exceptionReason);

    /**
     * 移除标记异常
     *
     * @param payLogNotifyId
     *         支付记录通知ID
     */
    void removeExceptionMark(@Param("payLogNotifyId") Long payLogNotifyId);
}
