/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.service.impl;

import net.guerlab.smart.pay.core.exception.*;
import net.guerlab.smart.pay.core.searchparams.AlipayConfigSearchParams;
import net.guerlab.smart.pay.service.entity.AlipayConfig;
import net.guerlab.smart.pay.service.mapper.AlipayConfigMapper;
import net.guerlab.smart.pay.service.service.AlipayConfigService;
import net.guerlab.smart.platform.server.service.BaseServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 支付宝支付配置服务实现
 *
 * @author guer
 */
@Service
public class AlipayConfigServiceImpl extends BaseServiceImpl<AlipayConfig, String, AlipayConfigMapper, AlipayConfigSearchParams>
        implements AlipayConfigService {

    @Override
    protected void insertBefore(AlipayConfig entity) {
        String appId = StringUtils.trimToNull(entity.getAppId());
        String appName = StringUtils.trimToNull(entity.getAppName());
        String privateKey = StringUtils.trimToNull(entity.getPrivateKey());
        String alipayPublicKey = StringUtils.trimToNull(entity.getAlipayPublicKey());

        if (appId == null) {
            throw new AlipayConfigAppIdInvalidException();
        } else if (selectById(appId) != null) {
            throw new AlipayConfigAppIdRepeatException();
        }
        if (appName == null) {
            throw new AlipayConfigAppNameInvalidException();
        } else if (appName.length() > APP_NAME_MAX_LENGTH) {
            throw new AlipayConfigAppNameLengthErrorException();
        }
        if (privateKey == null) {
            throw new AlipayConfigPrivateKeyInvalidException();
        }
        if (alipayPublicKey == null) {
            throw new AlipayConfigAlipayPublicKeyInvalidException();
        }

        entity.setAppId(appId);
        entity.setAppName(appName);
        entity.setPrivateKey(privateKey);
        entity.setAlipayPublicKey(alipayPublicKey);
        if (entity.getEnable() == null) {
            entity.setEnable(true);
        }
        if (entity.getEnableDevEnv() == null) {
            entity.setEnableDevEnv(false);
        }
    }

    @Override
    protected void updateBefore(AlipayConfig entity) {
        String appName = StringUtils.trimToNull(entity.getAppName());
        if (appName != null && appName.length() > APP_NAME_MAX_LENGTH) {
            throw new AlipayConfigAppNameLengthErrorException();
        }
        entity.setAppName(appName);
        entity.setPrivateKey(StringUtils.trimToNull(entity.getPrivateKey()));
        entity.setAlipayPublicKey(StringUtils.trimToNull(entity.getAlipayPublicKey()));
    }
}
