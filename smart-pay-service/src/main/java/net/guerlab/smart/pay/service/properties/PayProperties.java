/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 支付设置
 *
 * @author guer
 */
@Data
@RefreshScope
@ConfigurationProperties(prefix = "pay")
public class PayProperties {

    /**
     * 超时时间(分钟)
     */
    private long timeoutMinute = 15L;

    /**
     * 通知地址
     */
    private String notifyUrl;
}
