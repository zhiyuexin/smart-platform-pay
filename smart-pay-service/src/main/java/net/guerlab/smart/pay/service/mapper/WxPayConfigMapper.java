/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.guerlab.smart.pay.service.entity.WxPayConfig;

/**
 * 微信支付配置mapper
 *
 * @author guer
 */
public interface WxPayConfigMapper extends BaseMapper<WxPayConfig> {}
