/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import net.guerlab.smart.pay.core.domain.PayLogNotifyDTO;
import net.guerlab.spring.commons.dto.DefaultConvert;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 支付记录通知
 *
 * @author guer
 */
@Data
@TableName("pay_log_notify")
public class PayLogNotify implements DefaultConvert<PayLogNotifyDTO> {

    /**
     * 支付记录通知ID
     */
    @Id
    private Long payLogNotifyId;

    /**
     * 支付记录ID
     */
    @Column(name = "payLogId", nullable = false, updatable = false)
    private Long payLogId;

    /**
     * 支付订单ID
     */
    @Column(name = "payOrderId", nullable = false, updatable = false)
    private Long payOrderId;

    /**
     * 业务分组
     */
    @Column(name = "businessGroup", nullable = false, updatable = false)
    private String businessGroup;

    /**
     * 业务ID
     */
    @Column(name = "businessId", nullable = false, updatable = false)
    private String businessId;

    /**
     * 支付渠道
     */
    @Column(name = "payChannel", nullable = false, updatable = false)
    private String payChannel;

    /**
     * 金额
     */
    @Column(name = "amount", nullable = false, updatable = false)
    private BigDecimal amount;

    /**
     * 外部订单编号
     */
    @Column(name = "outOrderSn", nullable = false, updatable = false)
    private String outOrderSn;

    /**
     * 创建时间
     */
    @Column(name = "createTime", nullable = false, updatable = false)
    private LocalDateTime createTime;

    /**
     * 异常标志
     */
    @Column(name = "exceptionFlag", nullable = false)
    private Boolean exceptionFlag;

    /**
     * 异常原因
     */
    @Column(name = "exceptionReason", insertable = false)
    private String exceptionReason;

    /**
     * 通过支付记录构造支付记录通知
     *
     * @param payLog
     *         支付记录
     * @return 支付记录通知
     */
    public static PayLogNotify buildByPayLog(PayLog payLog) {
        PayLogNotify payLogNotify = new PayLogNotify();
        payLogNotify.setPayLogId(payLog.getPayLogId());
        payLogNotify.setPayOrderId(payLog.getPayOrderId());
        payLogNotify.setBusinessGroup(payLog.getBusinessGroup());
        payLogNotify.setBusinessId(payLog.getBusinessId());
        payLogNotify.setPayChannel(payLog.getPayChannel());
        payLogNotify.setAmount(payLog.getAmount());

        return payLogNotify;
    }
}
