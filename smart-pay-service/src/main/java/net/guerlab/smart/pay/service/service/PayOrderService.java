/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.service;

import net.guerlab.smart.pay.core.searchparams.PayOrderSearchParams;
import net.guerlab.smart.pay.service.entity.PayOrder;
import net.guerlab.smart.platform.server.service.BaseService;

/**
 * 支付订单服务
 *
 * @author guer
 */
public interface PayOrderService extends BaseService<PayOrder, Long, PayOrderSearchParams> {

    /**
     * 标记异常
     *
     * @param payOrderId
     *         支付订单ID
     * @param exceptionReason
     *         异常ID
     */
    void markException(Long payOrderId, String exceptionReason);

    /**
     * 移除标记异常
     *
     * @param payOrderId
     *         支付订单ID
     */
    void removeExceptionMark(Long payOrderId);

    /**
     * 获取实体类型
     *
     * @return 实体类型
     */
    @Override
    default Class<PayOrder> getEntityClass() {
        return PayOrder.class;
    }
}
