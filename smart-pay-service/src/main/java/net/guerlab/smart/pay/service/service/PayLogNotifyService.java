/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.service;

import net.guerlab.smart.pay.core.searchparams.PayLogNotifySearchParams;
import net.guerlab.smart.pay.service.entity.PayLogNotify;
import net.guerlab.smart.platform.server.service.BaseFindService;
import net.guerlab.smart.platform.server.service.BaseSaveService;

/**
 * 支付记录通知服务
 *
 * @author guer
 */
public interface PayLogNotifyService extends BaseFindService<PayLogNotify, Long, PayLogNotifySearchParams>, BaseSaveService<PayLogNotify, PayLogNotifySearchParams> {

    /**
     * 标记异常
     *
     * @param payLogId
     *         支付记录ID
     * @param exceptionReason
     *         异常ID
     */
    void markException(Long payLogId, String exceptionReason);

    /**
     * 移除标记异常
     *
     * @param payLogId
     *         支付记录ID
     */
    void removeExceptionMark(Long payLogId);

    /**
     * 获取实体类型
     *
     * @return 实体类型
     */
    @Override
    default Class<PayLogNotify> getEntityClass() {
        return PayLogNotify.class;
    }
}
