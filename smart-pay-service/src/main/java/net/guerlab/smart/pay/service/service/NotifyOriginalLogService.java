/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.service.service;

import net.guerlab.smart.pay.core.searchparams.NotifyOriginalLogSearchParams;
import net.guerlab.smart.pay.service.entity.NotifyOriginalLog;
import net.guerlab.smart.platform.server.service.BaseFindService;

/**
 * 通知原始记录服务
 *
 * @author guer
 */
public interface NotifyOriginalLogService extends BaseFindService<NotifyOriginalLog, Long, NotifyOriginalLogSearchParams> {

    /**
     * 添加记录
     *
     * @param payChannel
     *         支付渠道
     * @param originalData
     *         原始数据
     * @param result
     *         响应数据
     */
    void add(String payChannel, Object originalData, String result);

    /**
     * 获取实体类型
     *
     * @return 实体类型
     */
    @Override
    default Class<NotifyOriginalLog> getEntityClass() {
        return NotifyOriginalLog.class;
    }
}
