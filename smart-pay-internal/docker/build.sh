#!/bin/bash
name="smart-pay-internal"
version="21.0.0"
cp ../target/${name}-exec.jar app.jar
docker rmi -f ${DOCKER_REPOSITORY}/${name}:${version}
docker build --build-arg DOCKER_BASIC_IMAGE="${DOCKER_BASIC_IMAGE}" -t ${DOCKER_REPOSITORY}/${name}:${version} .
docker push ${DOCKER_REPOSITORY}/${name}:${version}
