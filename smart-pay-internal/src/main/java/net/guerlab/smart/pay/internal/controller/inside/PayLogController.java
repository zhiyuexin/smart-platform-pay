/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.internal.controller.inside;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.guerlab.smart.pay.core.domain.PayLogDTO;
import net.guerlab.smart.pay.core.exception.PayLogInvalidException;
import net.guerlab.smart.pay.core.searchparams.PayLogSearchParams;
import net.guerlab.smart.pay.service.service.PayLogService;
import net.guerlab.smart.platform.commons.util.BeanConvertUtils;
import net.guerlab.web.result.ListObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * 支付记录
 *
 * @author guer
 */
@Tag(name = "支付记录")
@RestController("/inside/payLog")
@RequestMapping("/inside/payLog")
public class PayLogController {

    private PayLogService service;

    @Operation(description = "根据支付记录ID查询支付记录")
    @GetMapping("/{id}")
    public PayLogDTO findOne(@Parameter(name = "id", required = true) @PathVariable Long id) {
        return service.selectByIdOptional(id).orElseThrow(PayLogInvalidException::new).convert();
    }

    @Operation(description = "根据搜索条件查询支付记录分页列表")
    @PostMapping
    public ListObject<PayLogDTO> findList(@RequestBody PayLogSearchParams searchParams) {
        return BeanConvertUtils.toListObject(service.selectPage(searchParams));
    }

    @Operation(description = "根据搜索条件查询全部支付记录")
    @PostMapping("/all")
    public Collection<PayLogDTO> findAll(@RequestBody PayLogSearchParams searchParams) {
        return BeanConvertUtils.toList(service.selectAll(searchParams));
    }

    @Autowired
    public void setService(PayLogService service) {
        this.service = service;
    }
}
