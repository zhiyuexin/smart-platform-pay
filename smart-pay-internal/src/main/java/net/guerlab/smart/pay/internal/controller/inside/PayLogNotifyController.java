/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.internal.controller.inside;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.guerlab.smart.pay.core.domain.PayLogNotifyDTO;
import net.guerlab.smart.pay.core.exception.PayLogNotifyInvalidException;
import net.guerlab.smart.pay.core.searchparams.PayLogNotifySearchParams;
import net.guerlab.smart.pay.service.service.PayLogNotifyService;
import net.guerlab.smart.platform.commons.util.BeanConvertUtils;
import net.guerlab.web.result.ListObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * 支付记录通知服务
 *
 * @author guer
 */
@Tag(name = "支付记录通知")
@RestController("/inside/payLogNotify")
@RequestMapping("/inside/payLogNotify")
public class PayLogNotifyController {

    private PayLogNotifyService service;

    @Operation(description = "根据支付记录通知ID查询支付记录通知")
    @GetMapping("/{id}")
    public PayLogNotifyDTO findOne(@Parameter(name = "id", required = true) @PathVariable Long id) {
        return service.selectByIdOptional(id).orElseThrow(PayLogNotifyInvalidException::new).convert();
    }

    @Operation(description = "根据搜索条件查询支付记录通知分页列表")
    @PostMapping
    public ListObject<PayLogNotifyDTO> findList(@RequestBody PayLogNotifySearchParams searchParams) {
        return BeanConvertUtils.toListObject(service.selectPage(searchParams));
    }

    @Operation(description = "根据搜索条件查询全部支付记录通知")
    @PostMapping("/all")
    public Collection<PayLogNotifyDTO> findAll(@RequestBody PayLogNotifySearchParams searchParams) {
        return BeanConvertUtils.toList(service.selectAll(searchParams));
    }

    @Autowired
    public void setService(PayLogNotifyService service) {
        this.service = service;
    }
}
