/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.internal.controller.inside;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.guerlab.smart.pay.core.domain.PayOrderDTO;
import net.guerlab.smart.pay.core.exception.PayOrderInvalidException;
import net.guerlab.smart.pay.core.searchparams.PayOrderSearchParams;
import net.guerlab.smart.pay.service.entity.PayOrder;
import net.guerlab.smart.pay.service.service.PayOrderService;
import net.guerlab.smart.platform.commons.util.BeanConvertUtils;
import net.guerlab.web.result.ListObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * 支付订单
 *
 * @author guer
 */
@Tag(name = "支付订单")
@RestController("/inside/payOrder")
@RequestMapping("/inside/payOrder")
public class PayOrderController {

    private PayOrderService service;

    @Operation(description = "根据支付订单ID查询支付订单")
    @GetMapping("/{id}")
    public PayOrderDTO findOne(@Parameter(name = "id", required = true) @PathVariable Long id) {
        return service.selectByIdOptional(id).orElseThrow(PayOrderInvalidException::new).convert();
    }

    @Operation(description = "根据搜索条件查询支付订单分页列表")
    @PostMapping
    public ListObject<PayOrderDTO> findList(@RequestBody PayOrderSearchParams searchParams) {
        return BeanConvertUtils.toListObject(service.selectPage(searchParams));
    }

    @Operation(description = "根据搜索条件查询全部支付订单")
    @PostMapping("/all")
    public Collection<PayOrderDTO> findAll(@RequestBody PayOrderSearchParams searchParams) {
        return BeanConvertUtils.toList(service.selectAll(searchParams));
    }

    @Operation(description = "添加支付订单")
    @PutMapping
    public PayOrderDTO add(@RequestBody PayOrderDTO dto) {
        PayOrder order = new PayOrder();
        BeanUtils.copyProperties(dto, order);
        service.insert(order);
        return order.convert();
    }

    @Autowired
    public void setService(PayOrderService service) {
        this.service = service;
    }
}
