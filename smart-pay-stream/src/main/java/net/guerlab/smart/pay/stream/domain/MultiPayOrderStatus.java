/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.stream.domain;

import lombok.Data;
import net.guerlab.smart.pay.core.enums.PayStatus;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * 多订单状态变更
 *
 * @author guer
 */
@Data
public class MultiPayOrderStatus {

    /**
     * 订单ID集合
     */
    private Collection<Long> payOrderIds;

    /**
     * 变更后的订单状态
     */
    private PayStatus payStatus;

    /**
     * 变更时间
     */
    private LocalDateTime changeTime;
}
