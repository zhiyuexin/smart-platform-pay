/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.stream.binders;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * 批量支付订单状态变更消息发送者
 * <p>
 * bean: net.guerlab.smart.hospital.pay.stream.domain.MultiPayOrderStatus
 *
 * @author guer
 */
public interface MultiPayOrderStatusChangeSenderChannel {

    /**
     * binding名称
     */
    @SuppressWarnings("WeakerAccess")
    String NAME = "pay_order_multi_status_change_output";

    /**
     * 构造批量支付订单状态变更消息发送者
     *
     * @return 批量支付订单状态变更消息发送者
     */
    @Output(NAME)
    MessageChannel output();
}
