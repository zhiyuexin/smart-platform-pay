/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.api.autoconfig;

import lombok.AllArgsConstructor;
import net.guerlab.smart.pay.api.PayLogNotifyApi;
import net.guerlab.smart.pay.api.feign.FeignPayLogNotifyApi;
import net.guerlab.smart.pay.core.domain.PayLogNotifyDTO;
import net.guerlab.smart.pay.core.exception.PayLogNotifyInvalidException;
import net.guerlab.smart.pay.core.searchparams.PayLogNotifySearchParams;
import net.guerlab.spring.searchparams.SearchParamsUtils;
import net.guerlab.web.result.ListObject;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * 支付记录通知服务接口feign实现
 *
 * @author guer
 */
@Configuration
@AutoConfigureAfter(PayLogNotifyApiLocalServiceAutoConfigure.class)
public class PayLogNotifyApiFeignAutoConfigure {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    @ConditionalOnMissingBean(PayLogNotifyApi.class)
    public PayLogNotifyApi payLogNotifyApiFeignWrapper(FeignPayLogNotifyApi api) {
        return new PayLogNotifyApiFeignWrapper(api);
    }

    @AllArgsConstructor
    static class PayLogNotifyApiFeignWrapper implements PayLogNotifyApi {

        private final FeignPayLogNotifyApi api;

        @Override
        public PayLogNotifyDTO findOne(Long payLogNotifyId) {
            return Optional.ofNullable(api.findOne(payLogNotifyId).getData()).orElseThrow(PayLogNotifyInvalidException::new);
        }

        @Override
        public ListObject<PayLogNotifyDTO> findList(PayLogNotifySearchParams searchParams) {
            HashMap<String, Object> params = new HashMap<>(8);
            SearchParamsUtils.handler(searchParams, params);
            return Optional.ofNullable(api.findList(params).getData()).orElse(ListObject.empty());
        }

        @Override
        public List<PayLogNotifyDTO> findAll(PayLogNotifySearchParams searchParams) {
            HashMap<String, Object> params = new HashMap<>(8);
            SearchParamsUtils.handler(searchParams, params);
            return Optional.ofNullable(api.findAll(params).getData()).orElse(Collections.emptyList());
        }
    }

}
