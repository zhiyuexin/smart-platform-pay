/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.api.feign;

import net.guerlab.smart.pay.core.domain.PayLogNotifyDTO;
import net.guerlab.web.result.ListObject;
import net.guerlab.web.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * 支付记录通知服务接口
 *
 * @author guer
 */
@FeignClient(value = "pay-internal", path = "/inside/payLogNotify")
public interface FeignPayLogNotifyApi {

    /**
     * 根据支付记录通知id查询支付记录通知
     *
     * @param payLogNotifyId
     *         支付记录通知id
     * @return 支付记录通知
     */
    @GetMapping("/{payLogNotifyId}")
    Result<PayLogNotifyDTO> findOne(@PathVariable("payLogNotifyId") Long payLogNotifyId);

    /**
     * 根据搜索参数查询支付记录通知列表
     *
     * @param searchParams
     *         搜索参数
     * @return 支付记录通知列表
     */
    @PostMapping
    Result<ListObject<PayLogNotifyDTO>> findList(@RequestBody Map<String, Object> searchParams);

    /**
     * 根据搜索参数查询支付记录通知列表
     *
     * @param searchParams
     *         搜索参数
     * @return 支付记录通知列表
     */
    @PostMapping("/all")
    Result<List<PayLogNotifyDTO>> findAll(@RequestBody Map<String, Object> searchParams);

}
