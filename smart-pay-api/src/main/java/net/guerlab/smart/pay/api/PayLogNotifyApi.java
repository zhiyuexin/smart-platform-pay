/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.api;

import net.guerlab.smart.pay.core.domain.PayLogNotifyDTO;
import net.guerlab.smart.pay.core.searchparams.PayLogNotifySearchParams;
import net.guerlab.web.result.ListObject;

import java.util.List;

/**
 * 支付记录通知服务接口
 *
 * @author guer
 */
public interface PayLogNotifyApi {

    /**
     * 根据支付记录通知id查询支付记录通知
     *
     * @param payLogNotifyId
     *         支付记录通知id
     * @return 支付记录通知
     */
    PayLogNotifyDTO findOne(Long payLogNotifyId);

    /**
     * 根据搜索参数查询支付记录通知列表
     *
     * @param searchParams
     *         搜索参数
     * @return 支付记录通知列表
     */
    ListObject<PayLogNotifyDTO> findList(PayLogNotifySearchParams searchParams);

    /**
     * 根据搜索参数查询支付记录通知列表
     *
     * @param searchParams
     *         搜索参数
     * @return 支付记录通知列表
     */
    List<PayLogNotifyDTO> findAll(PayLogNotifySearchParams searchParams);
}
