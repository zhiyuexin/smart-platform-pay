/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.api.feign;

import net.guerlab.smart.pay.core.domain.PayOrderDTO;
import net.guerlab.web.result.ListObject;
import net.guerlab.web.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 支付订单服务接口
 *
 * @author guer
 */
@FeignClient(value = "pay-internal", path = "/inside/payOrder")
public interface FeignPayOrderApi {

    /**
     * 根据支付订单id查询支付订单
     *
     * @param payOrderId
     *         支付订单id
     * @return 支付订单
     */
    @GetMapping("/{payOrderId}")
    Result<PayOrderDTO> findOne(@PathVariable("payOrderId") Long payOrderId);

    /**
     * 根据搜索参数查询支付订单列表
     *
     * @param searchParams
     *         搜索参数
     * @return 支付订单列表
     */
    @PostMapping
    Result<ListObject<PayOrderDTO>> findList(@RequestBody Map<String, Object> searchParams);

    /**
     * 根据搜索参数查询支付订单列表
     *
     * @param searchParams
     *         搜索参数
     * @return 支付订单列表
     */
    @PostMapping("/all")
    Result<List<PayOrderDTO>> findAll(@RequestBody Map<String, Object> searchParams);

    /**
     * 添加支付订单
     *
     * @param payOrder
     *         支付订单
     * @return 支付订单
     */
    @PutMapping
    Result<PayOrderDTO> add(@RequestBody PayOrderDTO payOrder);
}
