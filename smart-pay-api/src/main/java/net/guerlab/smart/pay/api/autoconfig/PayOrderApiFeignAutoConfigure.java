/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.api.autoconfig;

import lombok.AllArgsConstructor;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.smart.pay.api.PayOrderApi;
import net.guerlab.smart.pay.api.feign.FeignPayOrderApi;
import net.guerlab.smart.pay.core.domain.PayOrderDTO;
import net.guerlab.smart.pay.core.exception.PayOrderInvalidException;
import net.guerlab.smart.pay.core.searchparams.PayOrderSearchParams;
import net.guerlab.spring.searchparams.SearchParamsUtils;
import net.guerlab.web.result.ListObject;
import net.guerlab.web.result.Result;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * 支付订单服务接口feign实现
 *
 * @author guer
 */
@Configuration
@AutoConfigureAfter(PayOrderApiLocalServiceAutoConfigure.class)
public class PayOrderApiFeignAutoConfigure {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    @ConditionalOnMissingBean(PayOrderApi.class)
    public PayOrderApi payOrderApiFeignWrapper(FeignPayOrderApi api) {
        return new PayOrderApiFeignWrapper(api);
    }

    @AllArgsConstructor
    private static class PayOrderApiFeignWrapper implements PayOrderApi {

        private final FeignPayOrderApi api;

        @Override
        public PayOrderDTO findOne(Long payOrderId) {
            return Optional.ofNullable(api.findOne(payOrderId).getData()).orElseThrow(PayOrderInvalidException::new);
        }

        @Override
        public ListObject<PayOrderDTO> findList(PayOrderSearchParams searchParams) {
            HashMap<String, Object> params = new HashMap<>(8);
            SearchParamsUtils.handler(searchParams, params);
            return Optional.ofNullable(api.findList(params).getData()).orElse(ListObject.empty());
        }

        @Override
        public List<PayOrderDTO> findAll(PayOrderSearchParams searchParams) {
            HashMap<String, Object> params = new HashMap<>(8);
            SearchParamsUtils.handler(searchParams, params);
            return Optional.ofNullable(api.findAll(params).getData()).orElse(Collections.emptyList());
        }

        @Override
        public PayOrderDTO add(PayOrderDTO payOrder) {
            Result<PayOrderDTO> result = api.add(payOrder);

            if (!result.isStatus()) {
                throw new ApplicationException(result.getMessage(), result.getErrorCode());
            }

            return result.getData();
        }
    }

}
