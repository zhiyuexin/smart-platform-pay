/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.api;

import net.guerlab.smart.pay.core.domain.PayLogDTO;
import net.guerlab.smart.pay.core.searchparams.PayLogSearchParams;
import net.guerlab.web.result.ListObject;

import java.util.List;

/**
 * 支付记录服务接口
 *
 * @author guer
 */
public interface PayLogApi {

    /**
     * 根据支付记录id查询支付记录
     *
     * @param payLogId
     *         支付记录id
     * @return 支付记录
     */
    PayLogDTO findOne(Long payLogId);

    /**
     * 根据搜索参数查询支付记录列表
     *
     * @param searchParams
     *         搜索参数
     * @return 支付记录列表
     */
    ListObject<PayLogDTO> findList(PayLogSearchParams searchParams);

    /**
     * 根据搜索参数查询支付记录列表
     *
     * @param searchParams
     *         搜索参数
     * @return 支付记录列表
     */
    List<PayLogDTO> findAll(PayLogSearchParams searchParams);
}
