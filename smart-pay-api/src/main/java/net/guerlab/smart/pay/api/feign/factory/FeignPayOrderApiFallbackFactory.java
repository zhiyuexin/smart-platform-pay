/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.api.feign.factory;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.guerlab.smart.pay.api.feign.FeignPayOrderApi;
import net.guerlab.smart.pay.core.domain.PayOrderDTO;
import net.guerlab.web.result.Fail;
import net.guerlab.web.result.ListObject;
import net.guerlab.web.result.Result;
import org.springframework.cloud.openfeign.FallbackFactory;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 支付订单服务接口快速失败类构建工厂
 *
 * @author guer
 */
public class FeignPayOrderApiFallbackFactory implements FallbackFactory<FeignPayOrderApi> {

    @Override
    public FeignPayOrderApi create(Throwable cause) {
        return new FeignPayOrderApiFallback(cause);
    }

    /**
     * 支付订单服务接口快速失败实现
     */
    @Slf4j
    @AllArgsConstructor
    static class FeignPayOrderApiFallback implements FeignPayOrderApi {

        private final Throwable cause;

        @Override
        public Result<PayOrderDTO> findOne(Long payOrderId) {
            log.error("findOne fallback", cause);
            return new Fail<>("fallback");
        }

        @Override
        public Result<ListObject<PayOrderDTO>> findList(Map<String, Object> searchParams) {
            log.error("findOne findList", cause);
            return new Fail<>("fallback", ListObject.empty());
        }

        @Override
        public Result<List<PayOrderDTO>> findAll(Map<String, Object> searchParams) {
            log.error("findOne findAll", cause);
            return new Fail<>("fallback", Collections.emptyList());
        }

        @Override
        public Result<PayOrderDTO> add(PayOrderDTO payOrder) {
            log.error("add fallback", cause);
            return new Fail<>("fallback");
        }
    }

}
