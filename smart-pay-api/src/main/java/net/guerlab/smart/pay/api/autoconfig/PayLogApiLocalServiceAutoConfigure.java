/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.api.autoconfig;

import lombok.AllArgsConstructor;
import net.guerlab.smart.pay.api.PayLogApi;
import net.guerlab.smart.pay.core.domain.PayLogDTO;
import net.guerlab.smart.pay.core.exception.PayLogInvalidException;
import net.guerlab.smart.pay.core.searchparams.PayLogSearchParams;
import net.guerlab.smart.pay.service.service.PayLogService;
import net.guerlab.smart.platform.commons.util.BeanConvertUtils;
import net.guerlab.web.result.ListObject;
import org.springframework.context.annotation.*;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.lang.NonNull;

import java.util.List;

/**
 * 支付记录服务接口本地实现
 *
 * @author guer
 */
@Configuration
@Conditional(PayLogApiLocalServiceAutoConfigure.WrapperCondition.class)
public class PayLogApiLocalServiceAutoConfigure {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    public PayLogApi payLogApiLocalServiceWrapper(PayLogService service) {
        return new PayLogApiLocalServiceWrapper(service);
    }

    public static class WrapperCondition implements Condition {

        @Override
        public boolean matches(@NonNull ConditionContext context, @NonNull AnnotatedTypeMetadata metadata) {
            try {
                return WrapperCondition.class.getClassLoader().loadClass("net.guerlab.smart.pay.service.service.PayLogService") != null;
            } catch (Exception e) {
                return false;
            }
        }
    }

    @AllArgsConstructor
    private static class PayLogApiLocalServiceWrapper implements PayLogApi {

        private final PayLogService service;

        @Override
        public PayLogDTO findOne(Long payLogId) {
            return service.selectByIdOptional(payLogId).orElseThrow(PayLogInvalidException::new).convert();
        }

        @Override
        public ListObject<PayLogDTO> findList(PayLogSearchParams searchParams) {
            return BeanConvertUtils.toListObject(service.selectPage(searchParams));
        }

        @Override
        public List<PayLogDTO> findAll(PayLogSearchParams searchParams) {
            return BeanConvertUtils.toList(service.selectAll(searchParams));
        }
    }

}
