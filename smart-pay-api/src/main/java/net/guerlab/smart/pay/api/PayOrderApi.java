/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.api;

import net.guerlab.smart.pay.core.domain.PayOrderDTO;
import net.guerlab.smart.pay.core.searchparams.PayOrderSearchParams;
import net.guerlab.web.result.ListObject;

import java.util.List;

/**
 * 支付订单服务接口
 *
 * @author guer
 */
public interface PayOrderApi {

    /**
     * 根据支付订单id查询支付订单
     *
     * @param payOrderId
     *         支付订单id
     * @return 支付订单
     */
    PayOrderDTO findOne(Long payOrderId);

    /**
     * 根据搜索参数查询支付订单列表
     *
     * @param searchParams
     *         搜索参数
     * @return 支付订单列表
     */
    ListObject<PayOrderDTO> findList(PayOrderSearchParams searchParams);

    /**
     * 根据搜索参数查询支付订单列表
     *
     * @param searchParams
     *         搜索参数
     * @return 支付订单列表
     */
    List<PayOrderDTO> findAll(PayOrderSearchParams searchParams);

    /**
     * 添加支付订单
     *
     * @param payOrder
     *         支付订单
     * @return 支付订单
     */
    PayOrderDTO add(PayOrderDTO payOrder);
}
