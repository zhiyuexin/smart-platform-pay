/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.controller.user;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.smart.pay.core.domain.PayLogNotifyDTO;
import net.guerlab.smart.pay.core.exception.ExceptionReasonInvalidException;
import net.guerlab.smart.pay.core.exception.ExceptionReasonLengthErrorException;
import net.guerlab.smart.pay.core.exception.PayLogNotifyInvalidException;
import net.guerlab.smart.pay.core.searchparams.PayLogNotifySearchParams;
import net.guerlab.smart.pay.service.entity.PayLogNotify;
import net.guerlab.smart.pay.service.service.PayLogNotifyService;
import net.guerlab.smart.pay.web.domain.ExceptionDTO;
import net.guerlab.smart.pay.web.excel.PayLogNotifyExcelExport;
import net.guerlab.smart.platform.commons.Constants;
import net.guerlab.smart.platform.excel.ExcelUtils;
import net.guerlab.smart.platform.server.controller.BaseFindController;
import net.guerlab.smart.user.api.OperationLogApi;
import net.guerlab.smart.user.auth.UserContextHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * 支付记录通知
 *
 * @author guer
 */
@Tag(name = "支付记录通知")
@RestController("/user/payLogNotify")
@RequestMapping("/user/payLogNotify")
public class PayLogNotifyController extends BaseFindController<PayLogNotifyDTO, PayLogNotify, PayLogNotifyService, PayLogNotifySearchParams, Long> {

    private OperationLogApi operationLogApi;

    @Operation(description = "导出Excel", security = @SecurityRequirement(name = Constants.TOKEN))
    @GetMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response, PayLogNotifySearchParams searchParams) {
        beforeFind(searchParams);
        ExcelUtils.exportExcel(response, getService().selectAll(searchParams), PayLogNotifyExcelExport.class,
                "PayLogNotify-" + System.currentTimeMillis());
        operationLogApi.add("导出支付记录通知列表", UserContextHandler.getUserId(), searchParams);
    }

    @Operation(description = "标记异常", security = @SecurityRequirement(name = Constants.TOKEN))
    @PostMapping("/{id}/markException")
    @Transactional(rollbackFor = Exception.class)
    public PayLogNotifyDTO markException(@Parameter(name = "id", required = true) @PathVariable Long id, @RequestBody ExceptionDTO dto) {
        String reason = StringUtils.trimToNull(dto.getReason());

        if (reason == null) {
            throw new ExceptionReasonInvalidException();
        } else if (reason.length() > ExceptionDTO.REASON_MAX_LENGTH) {
            throw new ExceptionReasonLengthErrorException();
        }

        findOne0(id);
        getService().markException(id, reason);
        operationLogApi.add("标记异常-支付记录通知", UserContextHandler.getUserId(), id, reason);

        return findOne0(id).convert();
    }

    @Operation(description = "移除标记异常", security = @SecurityRequirement(name = Constants.TOKEN))
    @PostMapping("/{id}/removeMarkException")
    @Transactional(rollbackFor = Exception.class)
    public PayLogNotifyDTO removeMarkException(@Parameter(name = "id", required = true) @PathVariable Long id) {
        findOne0(id);
        getService().removeExceptionMark(id);
        operationLogApi.add("移除异常标记-支付记录通知", UserContextHandler.getUserId(), id);

        return findOne0(id).convert();
    }

    @Override
    protected ApplicationException nullPointException() {
        return new PayLogNotifyInvalidException();
    }

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    public void setOperationLogApi(OperationLogApi operationLogApi) {
        this.operationLogApi = operationLogApi;
    }
}
