/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.controller.user;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.smart.pay.core.domain.WxPayConfigDTO;
import net.guerlab.smart.pay.core.exception.WxPayConfigInvalidException;
import net.guerlab.smart.pay.core.searchparams.WxPayConfigSearchParams;
import net.guerlab.smart.pay.service.entity.WxPayConfig;
import net.guerlab.smart.pay.service.service.WxPayConfigService;
import net.guerlab.smart.platform.auth.annotation.IgnoreLogin;
import net.guerlab.smart.platform.commons.Constants;
import net.guerlab.smart.platform.server.controller.BaseController;
import net.guerlab.smart.user.api.OperationLogApi;
import net.guerlab.smart.user.auth.UserContextHandler;
import net.guerlab.web.result.Fail;
import net.guerlab.web.result.Result;
import net.guerlab.web.result.Succeed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;

/**
 * 微信支付配置
 *
 * @author guer
 */
@Tag(name = "微信支付配置")
@RestController("/user/wxPayConfig")
@RequestMapping("/user/wxPayConfig")
public class WxPayConfigController extends BaseController<WxPayConfigDTO, WxPayConfig, WxPayConfigService, WxPayConfigSearchParams, String> {

    private OperationLogApi operationLogApi;

    @IgnoreLogin
    @Operation(description = "解析证书文件base64")
    @PostMapping("/parseCertFile")
    public Result<String> parseCertFile(MultipartFile file) {
        try {
            return new Succeed<>(Succeed.MSG, Base64.getEncoder().encodeToString(file.getBytes()));
        } catch (Exception e) {
            return new Fail<>(e.getMessage());
        }
    }

    @Operation(description = "启用配置", security = @SecurityRequirement(name = Constants.TOKEN))
    @PostMapping("/{id}/enable")
    public WxPayConfigDTO enable(@Parameter(name = "id", required = true) @PathVariable String id) {
        WxPayConfig entity = findOne0(id);
        entity.setEnable(true);
        getService().updateById(entity);
        operationLogApi.add("启用微信支付配置", UserContextHandler.getUserId(), id);
        return entity.convert();
    }

    @Operation(description = "禁用配置", security = @SecurityRequirement(name = Constants.TOKEN))
    @PostMapping("/{id}/disable")
    public WxPayConfigDTO disable(@Parameter(name = "id", required = true) @PathVariable String id) {
        WxPayConfig entity = findOne0(id);
        entity.setEnable(false);
        getService().updateById(entity);
        operationLogApi.add("禁用微信支付配置", UserContextHandler.getUserId(), id);
        return entity.convert();
    }

    @Override
    protected ApplicationException nullPointException() {
        return new WxPayConfigInvalidException();
    }

    @Override
    public void copyProperties(WxPayConfigDTO dto, WxPayConfig entity, String id) {
        super.copyProperties(dto, entity, id);
        if (id != null) {
            entity.setAppId(id);
        }
    }

    @Override
    public void afterSave(WxPayConfig entity, WxPayConfigDTO dto) {
        operationLogApi.add("新增微信支付配置", UserContextHandler.getUserId(), entity);
    }

    @Override
    public void afterUpdate(WxPayConfig entity, WxPayConfigDTO dto) {
        operationLogApi.add("编辑微信支付配置", UserContextHandler.getUserId(), entity);
    }

    @Override
    public void afterDelete(WxPayConfig entity) {
        operationLogApi.add("删除微信支付配置", UserContextHandler.getUserId(), entity.getAppId());
    }

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    public void setOperationLogApi(OperationLogApi operationLogApi) {
        this.operationLogApi = operationLogApi;
    }
}
