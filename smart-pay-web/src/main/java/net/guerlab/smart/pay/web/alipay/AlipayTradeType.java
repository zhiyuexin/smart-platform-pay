/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.alipay;

import lombok.Getter;

/**
 * 支付宝支付类型
 *
 * @author guer
 */
@Getter
public enum AlipayTradeType {
    /**
     * APP
     */
    APP,
    /**
     * PC
     */
    PAGE,
    /**
     * 移动H5
     */
    WAP
}
