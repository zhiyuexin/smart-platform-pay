/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.controller.wx;

import io.swagger.v3.oas.annotations.tags.Tag;
import net.guerlab.smart.pay.web.controller.commons.notify.wx.WxPayH5ChannelNotifyController;
import net.guerlab.smart.pay.web.wx.AbstractWxPayRequestController;
import net.guerlab.smart.pay.web.wx.WxPayServiceType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付请求-微信-h5
 *
 * @author guer
 */
@Tag(name = "支付请求-微信-h5")
@RestController("/wx/payRequest/h5")
@RequestMapping("/wx/payRequest/h5")
public class WxPayH5ChannelPayController extends AbstractWxPayRequestController {

    @Override
    protected WxPayServiceType getWxPayServiceType() {
        return WxPayServiceType.H5;
    }

    @Override
    public String getNotifyUrl(String appId) {
        return WxPayH5ChannelNotifyController.URL + "/" + appId;
    }
}
