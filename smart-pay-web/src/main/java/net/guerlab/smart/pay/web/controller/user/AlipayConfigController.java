/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.controller.user;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.smart.pay.core.domain.AlipayConfigDTO;
import net.guerlab.smart.pay.core.exception.AlipayConfigInvalidException;
import net.guerlab.smart.pay.core.searchparams.AlipayConfigSearchParams;
import net.guerlab.smart.pay.service.entity.AlipayConfig;
import net.guerlab.smart.pay.service.service.AlipayConfigService;
import net.guerlab.smart.platform.commons.Constants;
import net.guerlab.smart.platform.server.controller.BaseController;
import net.guerlab.smart.user.api.OperationLogApi;
import net.guerlab.smart.user.auth.UserContextHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付宝支付配置
 *
 * @author guer
 */
@Tag(name = "支付宝支付配置")
@RestController("/user/alipayConfig")
@RequestMapping("/user/alipayConfig")
public class AlipayConfigController extends BaseController<AlipayConfigDTO, AlipayConfig, AlipayConfigService, AlipayConfigSearchParams, String> {

    private OperationLogApi operationLogApi;

    @Operation(description = "启用配置", security = @SecurityRequirement(name = Constants.TOKEN))
    @PostMapping("/{id}/enable")
    public AlipayConfigDTO enable(@Parameter(name = "id", required = true) @PathVariable String id) {
        AlipayConfig entity = findOne0(id);
        entity.setEnable(true);
        getService().updateById(entity);
        operationLogApi.add("启用支付宝支付配置", UserContextHandler.getUserId(), id);
        return entity.convert();
    }

    @Operation(description = "禁用配置", security = @SecurityRequirement(name = Constants.TOKEN))
    @PostMapping("/{id}/disable")
    public AlipayConfigDTO disable(@Parameter(name = "id", required = true) @PathVariable String id) {
        AlipayConfig entity = findOne0(id);
        entity.setEnable(false);
        getService().updateById(entity);
        operationLogApi.add("禁用支付宝支付配置", UserContextHandler.getUserId(), id);
        return entity.convert();
    }

    @Override
    protected ApplicationException nullPointException() {
        return new AlipayConfigInvalidException();
    }

    @Override
    public void copyProperties(AlipayConfigDTO dto, AlipayConfig entity, String id) {
        super.copyProperties(dto, entity, id);
        if (id != null) {
            entity.setAppId(id);
        }
    }

    @Override
    public void afterSave(AlipayConfig entity, AlipayConfigDTO dto) {
        operationLogApi.add("新增支付宝支付配置", UserContextHandler.getUserId(), entity);
    }

    @Override
    public void afterUpdate(AlipayConfig entity, AlipayConfigDTO dto) {
        operationLogApi.add("编辑支付宝支付配置", UserContextHandler.getUserId(), entity);
    }

    @Override
    public void afterDelete(AlipayConfig entity) {
        operationLogApi.add("删除支付宝支付配置", UserContextHandler.getUserId(), entity.getAppId());
    }

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    public void setOperationLogApi(OperationLogApi operationLogApi) {
        this.operationLogApi = operationLogApi;
    }
}
