/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.excel;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import net.guerlab.smart.pay.core.enums.PayStatus;

/**
 * 支付状态转换
 *
 * @author guer
 */
public class PayStatusConverter implements Converter<PayStatus> {

    @Override
    public Class supportJavaTypeKey() {
        return PayStatus.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public PayStatus convertToJavaData(CellData cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        switch (cellData.getStringValue()) {
            case "待支付":
                return PayStatus.WAIT_PAY;
            case "已完成":
                return PayStatus.PAYED;
            case "已超时":
                return PayStatus.TIMEOUT;
            case "已取消":
                return PayStatus.CANCELED;
            default:
                return null;
        }
    }

    @Override
    public CellData convertToExcelData(PayStatus value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        switch (value) {
            case WAIT_PAY:
                return new CellData("待支付");
            case PAYED:
                return new CellData("已完成");
            case TIMEOUT:
                return new CellData("已超时");
            case CANCELED:
                return new CellData("已取消");
            default:
                return null;
        }
    }
}
