/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.controller.commons.pay.wx;

import io.swagger.v3.oas.annotations.tags.Tag;
import net.guerlab.smart.pay.web.controller.commons.notify.wx.WxPayNativeChannelNotifyController;
import net.guerlab.smart.pay.web.wx.AbstractWxPayRequestController;
import net.guerlab.smart.pay.web.wx.WxPayServiceType;
import net.guerlab.smart.platform.auth.annotation.IgnoreLogin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付请求-微信-native
 *
 * @author guer
 */
@IgnoreLogin
@Tag(name = "支付请求-微信-native")
@RestController("/commons/payRequest/native")
@RequestMapping("/commons/payRequest/native")
public class WxPayNativeChannelPayController extends AbstractWxPayRequestController {

    @Override
    protected WxPayServiceType getWxPayServiceType() {
        return WxPayServiceType.NATIVE;
    }

    @Override
    public String getNotifyUrl(String appId) {
        return WxPayNativeChannelNotifyController.URL + "/" + appId;
    }
}
