/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.wx;

import java.math.BigDecimal;

/**
 * 微信支付服务常量
 *
 * @author guer
 */
public interface WxPayServiceConstant {

    /**
     * 常量-100
     */
    BigDecimal HUNDRED = BigDecimal.valueOf(100);

    /**
     * 交易类型-jsapi/小程序
     */
    String TRADE_TYPE_JSAPI = "JSAPI";

    /**
     * 交易类型-app
     */
    String TRADE_TYPE_APP = "APP";

    /**
     * 交易类型-native
     */
    String TRADE_TYPE_NATIVE = "NATIVE";

    /**
     * 交易类型-h5
     */
    String TRADE_TYPE_H5 = "MWEB";

}
