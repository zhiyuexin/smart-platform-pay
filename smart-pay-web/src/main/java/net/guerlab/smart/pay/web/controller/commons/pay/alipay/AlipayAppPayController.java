/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.controller.commons.pay.alipay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.smart.pay.service.entity.PayLog;
import net.guerlab.smart.pay.web.alipay.AbstractAlipayRequestController;
import net.guerlab.smart.pay.web.alipay.AlipayTradeType;
import net.guerlab.smart.pay.web.controller.commons.notify.alipay.AlipayAppChannelNotifyController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 支付请求-支付宝-app
 *
 * @author guer
 */
@Tag(name = "支付请求-支付宝-app")
@RestController(AlipayAppPayController.URL)
@RequestMapping(AlipayAppPayController.URL)
public class AlipayAppPayController extends AbstractAlipayRequestController {

    public static final String URL = "/commons/pay/alipay/app";

    @Override
    protected Object pay0(String appId, PayLog payLog, String returnUrl, HttpServletRequest request) {
        AlipayTradeAppPayModel bizModel = new AlipayTradeAppPayModel();
        bizModel.setOutTradeNo(String.valueOf(payLog.getPayLogId()));
        bizModel.setProductCode("QUICK_MSECURITY_PAY");
        bizModel.setTotalAmount(payLog.getAmount().toString());
        bizModel.setSubject(payLog.getOrderTitle());

        AlipayTradeAppPayRequest payRequest = new AlipayTradeAppPayRequest();
        payRequest.setNotifyUrl(payProperties.getNotifyUrl() + getNotifyUrl(appId));
        if (returnUrl != null) {
            payRequest.setReturnUrl(returnUrl);
        }
        payRequest.setBizModel(bizModel);
        try {
            return buildAlipayClient(appId, true).sdkExecute(payRequest);
        } catch (AlipayApiException e) {
            throw new ApplicationException(e.getErrCode() + ":" + e.getErrMsg());
        }
    }

    @Override
    protected String getNotifyUrl(String appId) {
        return AlipayAppChannelNotifyController.URL + "/" + appId;
    }

    @Override
    protected AlipayTradeType getAlipayTradeType() {
        return AlipayTradeType.APP;
    }
}
