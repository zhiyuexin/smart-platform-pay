/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.controller.user;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.smart.pay.core.domain.NotifyOriginalLogDTO;
import net.guerlab.smart.pay.core.exception.NotifyOriginalLogInvalidException;
import net.guerlab.smart.pay.core.searchparams.NotifyOriginalLogSearchParams;
import net.guerlab.smart.pay.service.entity.NotifyOriginalLog;
import net.guerlab.smart.pay.service.service.NotifyOriginalLogService;
import net.guerlab.smart.pay.web.excel.NotifyOriginalLogExcelExport;
import net.guerlab.smart.platform.commons.Constants;
import net.guerlab.smart.platform.excel.ExcelUtils;
import net.guerlab.smart.platform.server.controller.BaseFindController;
import net.guerlab.smart.user.api.OperationLogApi;
import net.guerlab.smart.user.auth.UserContextHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * 通知原始记录
 *
 * @author guer
 */
@Tag(name = "通知原始记录")
@RestController("/user/notifyOriginalLog")
@RequestMapping("/user/notifyOriginalLog")
public class NotifyOriginalLogController extends BaseFindController<NotifyOriginalLogDTO, NotifyOriginalLog, NotifyOriginalLogService, NotifyOriginalLogSearchParams, Long> {

    private OperationLogApi operationLogApi;

    @Operation(description = "导出Excel", security = @SecurityRequirement(name = Constants.TOKEN))
    @GetMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response, NotifyOriginalLogSearchParams searchParams) {
        beforeFind(searchParams);
        ExcelUtils.exportExcel(response, getService().selectAll(searchParams), NotifyOriginalLogExcelExport.class,
                "NotifyOriginalLog-" + System.currentTimeMillis());
        operationLogApi.add("导出通知原始记录列表", UserContextHandler.getUserId(), searchParams);
    }

    @Override
    protected ApplicationException nullPointException() {
        return new NotifyOriginalLogInvalidException();
    }

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    public void setOperationLogApi(OperationLogApi operationLogApi) {
        this.operationLogApi = operationLogApi;
    }
}
