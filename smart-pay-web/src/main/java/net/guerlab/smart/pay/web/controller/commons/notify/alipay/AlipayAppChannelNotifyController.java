/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.controller.commons.notify.alipay;

import io.swagger.v3.oas.annotations.tags.Tag;
import net.guerlab.smart.pay.web.alipay.AbstractAlipayNotifyController;
import net.guerlab.smart.pay.web.alipay.AlipayTradeType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付回调-支付宝-app
 *
 * @author guer
 */
@Tag(name = "支付回调-支付宝-app")
@RestController(AlipayAppChannelNotifyController.URL)
@RequestMapping(AlipayAppChannelNotifyController.URL)
public class AlipayAppChannelNotifyController extends AbstractAlipayNotifyController {

    public static final String URL = "/commons/notify/alipay/app";

    @Override
    protected AlipayTradeType getAlipayTradeType() {
        return AlipayTradeType.APP;
    }
}
