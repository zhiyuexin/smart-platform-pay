/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import net.guerlab.smart.pay.core.domain.PayLogExtends;
import net.guerlab.smart.pay.core.enums.PayStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 支付记录
 *
 * @author guer
 */
@Data
public class PayLogExcelExport {

    /**
     * 支付记录ID
     */
    @ExcelProperty(value = "支付记录ID", index = 0)
    private Long payLogId;

    /**
     * 支付订单ID
     */
    @ExcelProperty(value = "支付订单ID", index = 1)
    private Long payOrderId;

    /**
     * 订单标题
     */
    @ExcelProperty(value = "订单标题", index = 2)
    private String orderTitle;

    /**
     * 业务分组
     */
    @ExcelProperty(value = "业务分组", index = 3)
    private String businessGroup;

    /**
     * 业务ID
     */
    @ExcelProperty(value = "业务ID", index = 4)
    private String businessId;

    /**
     * 支付渠道
     */
    @ExcelProperty(value = "支付渠道", index = 5)
    private String payChannel;

    /**
     * 金额
     */
    @ExcelProperty(value = "金额", index = 6)
    private BigDecimal amount;

    /**
     * 支付状态
     */
    @ExcelProperty(value = "支付状态", index = 7, converter = PayStatusConverter.class)
    private PayStatus payStatus;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间", index = 8)
    private LocalDateTime createTime;

    /**
     * 支付完成时间
     */
    @ExcelProperty(value = "支付完成时间", index = 9)
    private LocalDateTime payedTime;

    /**
     * 扩展信息
     */
    @ExcelProperty(value = "扩展信息", index = 10, converter = PayLogExtendsConverter.class)
    private PayLogExtends extend;

    /**
     * 异常标志
     */
    @ExcelProperty(value = "异常标志", index = 11)
    private Boolean exceptionFlag;

    /**
     * 异常原因
     */
    @ExcelProperty(value = "异常原因", index = 12)
    private String exceptionReason;
}
