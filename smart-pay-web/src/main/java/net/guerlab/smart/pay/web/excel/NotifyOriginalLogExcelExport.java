/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 通知原始记录
 *
 * @author guer
 */
@Data
public class NotifyOriginalLogExcelExport {

    /**
     * 通知原始记录ID
     */
    @ExcelProperty(value = "通知原始记录ID", index = 0)
    private Long notifyOriginalLogId;

    /**
     * 支付渠道
     */
    @ExcelProperty(value = "支付渠道", index = 1)
    private String payChannel;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间", index = 2)
    private LocalDateTime createTime;

    /**
     * 内容
     */
    @ExcelProperty(value = "内容", index = 3)
    private String content;

    /**
     * 响应
     */
    @ExcelProperty(value = "响应", index = 4)
    private String result;
}
