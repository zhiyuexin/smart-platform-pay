/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 异常信息
 *
 * @author guer
 */
@Data
@Schema(name = "ExceptionDTO", description = "异常信息")
public class ExceptionDTO {

    /**
     * 原因最大长度
     */
    public static final int REASON_MAX_LENGTH = 255;

    /**
     * 原因
     */
    @Schema(description = "原因")
    private String reason;
}
