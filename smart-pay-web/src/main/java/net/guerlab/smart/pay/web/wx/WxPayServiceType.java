/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.wx;

import lombok.Getter;

/**
 * 微信支付服务类型
 *
 * @author guer
 */
@Getter
public enum WxPayServiceType {
    /**
     * JSAPI
     */
    JSAPI(WxPayServiceConstant.TRADE_TYPE_JSAPI),
    /**
     * NATIVE
     */
    NATIVE(WxPayServiceConstant.TRADE_TYPE_NATIVE),
    /**
     * APP
     */
    APP(WxPayServiceConstant.TRADE_TYPE_APP),
    /**
     * H5
     */
    H5(WxPayServiceConstant.TRADE_TYPE_H5),
    /**
     * 小程序
     */
    MINI_APP(WxPayServiceConstant.TRADE_TYPE_JSAPI);

    private final String tradeType;

    WxPayServiceType(String tradeType) {
        this.tradeType = tradeType;
    }
}
