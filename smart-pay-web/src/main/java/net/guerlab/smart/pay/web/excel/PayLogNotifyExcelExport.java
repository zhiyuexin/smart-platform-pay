/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 支付记录通知
 *
 * @author guer
 */
@Data
public class PayLogNotifyExcelExport {

    /**
     * 支付记录通知ID
     */
    @Id
    @ExcelProperty(value = "支付记录通知ID", index = 0)
    private Long payLogNotifyId;

    /**
     * 支付记录ID
     */
    @ExcelProperty(value = "支付记录ID", index = 1)
    private Long payLogId;

    /**
     * 支付订单ID
     */
    @ExcelProperty(value = "支付订单ID", index = 2)
    private Long payOrderId;

    /**
     * 业务分组
     */
    @ExcelProperty(value = "业务分组", index = 3)
    private String businessGroup;

    /**
     * 业务ID
     */
    @ExcelProperty(value = "业务ID", index = 4)
    private String businessId;

    /**
     * 支付渠道
     */
    @ExcelProperty(value = "支付渠道", index = 5)
    private String payChannel;

    /**
     * 金额
     */
    @ExcelProperty(value = "金额", index = 6)
    private BigDecimal amount;

    /**
     * 外部订单编号
     */
    @ExcelProperty(value = "外部订单编号", index = 7)
    private String outOrderSn;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间", index = 8)
    private LocalDateTime createTime;

    /**
     * 异常标志
     */
    @ExcelProperty(value = "异常标志", index = 9)
    private Boolean exceptionFlag;

    /**
     * 异常原因
     */
    @ExcelProperty(value = "异常原因", index = 10)
    private String exceptionReason;
}
