/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.smart.pay.web.alipay;

import com.alipay.api.AlipayObject;
import com.alipay.api.AlipayRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.slf4j.Slf4j;
import net.guerlab.smart.pay.core.domain.PayLogExtends;
import net.guerlab.smart.pay.service.entity.PayLog;
import net.guerlab.smart.pay.service.service.PayLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * 抽象支付宝支付请求控制器
 *
 * @author guer
 */
@Slf4j
public abstract class AbstractAlipayRequestController extends AbstractAlipayController {

    private PayLogService logService;

    @Operation(description = "通过支付订单ID请求支付")
    @GetMapping("/byPayOrderId/{appId}/{payOrderId}")
    public final Object byPayOrderId(@Parameter(name = "支付宝应用ID", required = true) @PathVariable String appId, @Parameter(name = "支付订单ID", required = true) @PathVariable Long payOrderId,
            @Parameter(name = "returnUrl") @RequestParam(required = false) String returnUrl, HttpServletRequest request) {
        PayLog payLog = logService.create(payOrderId, getPayChannel(), createPayLogExtends(appId));
        return pay0(appId, payLog, returnUrl, request);
    }

    @Operation(description = "通过业务分组和业务ID请求支付")
    @GetMapping("/byBusinessId/{appId}/{businessGroup}/{businessId}")
    public final Object byBusinessId(@Parameter(name = "支付宝应用ID", required = true) @PathVariable String appId, @Parameter(name = "业务分组", required = true) @PathVariable String businessGroup,
            @Parameter(name = "业务ID", required = true) @PathVariable String businessId, @Parameter(name = "returnUrl") @RequestParam(required = false) String returnUrl,
            HttpServletRequest request) {
        PayLog payLog = logService.create(businessGroup, businessId, getPayChannel(), createPayLogExtends(appId));
        return pay0(appId, payLog, returnUrl, request);
    }

    private PayLogExtends createPayLogExtends(String appId) {
        PayLogExtends payLogExtends = new PayLogExtends();
        payLogExtends.put(KEY_APP_ID, appId);
        return payLogExtends;
    }

    /**
     * 支付构造处理
     *
     * @param appId
     *         应用ID
     * @param payLog
     *         支付记录
     * @param returnUrl
     *         返回url
     * @param request
     *         请求对象
     * @return 响应
     */
    protected abstract Object pay0(String appId, PayLog payLog, String returnUrl, HttpServletRequest request);

    /**
     * 获取通知URL
     *
     * @param appId
     *         应用ID
     * @return 通知URL
     */
    protected abstract String getNotifyUrl(String appId);

    protected final void fillRequestData(String appId, String returnUrl, AlipayRequest<?> request, AlipayObject bizModel) {
        request.setNotifyUrl(payProperties.getNotifyUrl() + getNotifyUrl(appId));
        if (returnUrl != null) {
            request.setReturnUrl(returnUrl);
        }
        request.setBizModel(bizModel);
    }

    @Autowired
    public void setLogService(PayLogService logService) {
        this.logService = logService;
    }
}
